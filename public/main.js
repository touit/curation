var g_array = null;
var g_number = null;
var g_count = null;
var g_keeped = null;
var g_removed = null;
const cs = '_curation_state';

function sayError(e) {
    document.getElementById('console').textContent = 'Error: ' + e;
}

function loadfile() {
    let reader = new FileReader();
    let lf = document.getElementById('loadfile');
    let file = lf.files[0];
    reader.onload = function(e) {
        let json;
        try {
            json = JSON.parse(e.target.result);
        } catch(e) {
            sayError(e);
            return;
        }
        if (!Array.isArray(json)) {
            sayError('Not an array.');
            return;
        }
        if (json.length === 0) {
            sayError('No element in array.');
            return;
        } 
        g_array = json;
        g_count = g_array.length;
        document.getElementById('count').textContent = g_count;
        g_number = 0;
        g_keeped = 0;
        g_removed = 0;
        let ffund = false;
        g_array.forEach(function(e,i) {
            if (e['id'] == undefined) {
                sayError('There is no "id" property for element ' + i);
            }
            if (e[cs] === undefined) {
                if (!ffund) {
                    g_number = i;
                    ffund = true;
                }
            } else if(e[cs]) {
                g_keeped++;
            } else {
                g_removed++;
            }
        });
        document.getElementById('panel').style.display = 'inline';
        curation();
    };
    reader.readAsText(file);
}

function curation() {
    document.getElementById('container').textContent = '';
    let twid = g_array[g_number]['id'];
    if (twid == undefined) {
        sayError('There is no "id" property.')
        return;
    }
    document.getElementById('number').textContent = g_number;
    let state = g_array[g_number][cs];
    let bg;
    switch (state) {
        case undefined:
            state = 'undefined';
            bg = 'silver';
            break;
        case true:
            state = 'keep';
            bg = 'lime';
            break;
        case false:
            state = 'remove';
            bg = 'red';
            break;
    }
    let s = document.getElementById('state');
    s.textContent = state;
    s.style.backgroundColor = bg;
    updateCurated();
    twttr.ready(function(twttr) {
        twttr.widgets.createTweet(
            twid,
            document.getElementById('container'),
            {
                dnt: true
            }
        );
    });
}

function updateCurated() {
    document.getElementById('keeped').textContent = g_keeped;
    document.getElementById('removed').textContent = g_removed;
    let c = document.getElementById('curated');
    let curated = g_keeped + g_removed;
    c.textContent = curated;
    if (curated === g_count) {
        c.style.backgroundColor = 'lime';
    } else {
        c.style.backgroundColor = 'white';
    }
}

function keep() {
    switch(g_array[g_number][cs]) {
        case undefined:
            g_keeped++;
            break;
        case false:
            g_removed--;
            g_keeped++;

    }
    g_array[g_number][cs] = true;
    next();
}

function remove() {
    switch(g_array[g_number][cs]) {
        case undefined:
            g_removed++;
            break;
        case true:
            g_keeped--;
            g_removed++;

    }
    g_array[g_number][cs] = false;
    next();
}

function prev() {
    if (g_number === 0) {
        g_number = g_count - 1;
    } else {
        g_number--;
    }
    curation();
}

function next() {
    if (g_number === g_count - 1) {
        g_number = 0;
    } else {
        g_number++;
    }
    curation();
}

function save() {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:application/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(g_array,null,'  ')));
    element.setAttribute('download', 'curation_tweets.json');
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

function viewJson() {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:application/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(g_array[g_number],null,'  ')));
    element.setAttribute('target', '_blank');
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}

window.addEventListener("keydown", function (event) {
    if (event.defaultPrevented) {
        return;
    }
    switch (event.key) {
        case "ArrowUp":
            document.getElementById("keep").click();
            break;
        case "ArrowDown":
            document.getElementById("remove").click();
            break;
        case "ArrowLeft":
            document.getElementById("prev").click();
            break;
        case "ArrowRight":
            document.getElementById("next").click();
            break;
        default:
            return;
    }
    event.preventDefault();
}, false);
