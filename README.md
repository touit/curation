# Twarc2 curation

## Recipe

- query using twarc into queryname.json
- arrayfy using sed -i -f arrayfying queryname.json
- extract elements that are not retweets (ie tweets, reply and quotes). TODO: write a jq script for this.

You now have 2 files: queryname_no_RT.json and queryname_full.json

- manually curate non RTs

1/ either you have time or the dataset is small and you can fully curate it. splitting in small files (500) then
re-merge is a good idea. (`split` and TODO:merge are the programs)

2/ either not, then you can `sample` (randomly) some tweets from your dataset and perform curation on it.
You'll get a proportion of keeped/total and keep that number.

perform curation with touit.gitlab.io/curation

- extract necessary and sufficient data

for 1/
`forpandas_allcurated` on queryname_no_RT.json that have been fully curated into queryname_no_RT_forpandas.json
`extract_retweets_forpandas` on queryname_full.json into queryname_RT_forpandas.json

for 2/
`forpandas_proportion` on queryname_full.json

- run `jupyter notebook` and play with notebook.ipynb

